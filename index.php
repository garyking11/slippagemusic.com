<!doctype html>

<html lang="en">
<head>
    <meta charset="utf-8">

    <title>Slippage</title>
    <meta name="description" content="Slippage">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="/assets/css/local.css">

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

</head>

<body>
<div id="page-top">
    <div class="container">
        <img src="/assets/images/logo.png" alt="Slippage"/>
    </div>
</div>


<div class="container">

    <h1>Slippage website coming soon!!!</h1>

    <div class="callouts">
        <img src="/assets/images/allison.jpg" alt="Allison"/>
        <img src="/assets/images/gary.jpg" alt="Gary"/>
        <img src="/assets/images/jack.jpg" alt="Jack"/>
        <img src="/assets/images/scott.jpg" alt="Scott"/>
    </div>
</div>

</body>
</html>
