 <script type="text/javascript">

    function initialize() {
      if (GBrowserIsCompatible()) {
	var map = new GMap2(document.getElementById("map_canvas"));
    map.setCenter(new GLatLng(47.686709, -122.363654), 13);
    

//start new marker	
	var center = new GLatLng(47.686709, -122.363654);
	map.setCenter(center, 11);
	
	var marker0 = new GMarker(center, {title: "Avast! Recording"});
	
	GEvent.addListener(marker0, "mouseout", function() {
	  map.closeInfoWindow();
	  });
	
	GEvent.addListener(marker0, "mouseover", function() {
	  marker0.openInfoWindowHtml("Avast! Recording<br />601 NW 80th St<br />Seattle, WA 98117<br />206.633.3926");
	  });
	
	map.addOverlay(marker0);

//start new marker
//	var center = new GLatLng(47.137678, -122.476475);
//	//map.setCenter(center, 11);
//	
//	var marker1 = new GMarker(center, {title: "McChord Air Force Base"});
//	
//	GEvent.addListener(marker1, "mouseout", function() {
//	  map.closeInfoWindow();
//	  });
//	
//	GEvent.addListener(marker1, "mouseover", function() {
//	  marker1.openInfoWindowHtml("McChord Air Force Base");
//	  });
//	
//	map.addOverlay(marker1);
//	
////start new marker	
//	var center = new GLatLng(47.0617116, -122.583444);
//	//map.setCenter(center, 11);
//	
//	var marker2 = new GMarker(center, {title: "Ft Lewis"});
//	
//	GEvent.addListener(marker2, "mouseout", function() {
//	  map.closeInfoWindow();
//	  });
//	
//	GEvent.addListener(marker2, "mouseover", function() {
//	  marker2.openInfoWindowHtml("Ft Lewis");
//	  });
//	
//	map.addOverlay(marker2);
//	
	map.setUIToDefault();

      }
    }
    </script>
    

    
    <fieldset class="subset">
    <legend class="subset">Map</legend> 
    	<div id="map_canvas" style="width: 389px; height: 330px; margin: 0 auto; padding: 3px; border:1px solid #d8d8d8;"></div>
	</fieldset>